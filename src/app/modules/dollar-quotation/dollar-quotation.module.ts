import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DollarQuotationComponent } from './dollar-quotation.component';
import { ClrFormsNextModule } from '@clr/angular';
import { DollarQuotationService } from './dollar-quotation.service';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        DollarQuotationComponent
    ],
    imports: [
        CommonModule,
        ClrFormsNextModule,
        FormsModule
     ],
    exports: [
        DollarQuotationComponent
    ],
    providers: [
        DollarQuotationService
    ],
    entryComponents: [ DollarQuotationComponent ]
})

export class DollarQuotationModule {}
