import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

const QUOTATIONS: any = [
    {conversionRate: '3,70', conversionType: 'USD -> BRL', dateInitialValidity: '10/16/2018', dateFinallValidity: '10/16/2018'},
    {conversionRate: '3,56', conversionType: 'USD -> BRL', dateInitialValidity: '10/21/2018', dateFinallValidity: '10/26/2018'},
    {conversionRate: '3,89', conversionType: 'USD -> BRL', dateInitialValidity: '10/01/2018', dateFinallValidity: '10/16/2018'},
    {conversionRate: '3,70', conversionType: 'USD -> BRL', dateInitialValidity: '10/18/2018', dateFinallValidity: '10/21/2018'}
];


@Injectable()
export class DollarQuotationService {


    listQuotations(filter: any): Observable<any[]> {
        console.log(filter);
        return of(QUOTATIONS);
    }
}
