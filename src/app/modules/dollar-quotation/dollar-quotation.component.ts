import { Component, OnInit } from '@angular/core';
import { DollarQuotationService } from './dollar-quotation.service';

@Component({
    selector: 'app-dollar-quotation',
    templateUrl: './dollar-quotation.component.html',
    styleUrls: ['./dollar-quotation.component.scss']
})
export class DollarQuotationComponent implements OnInit {
    quotations: any = [];
    dateInitialValidity: any;
    dateFinallValidity: any;

    constructor(private readonly dollarService: DollarQuotationService) { }

    ngOnInit(): void { }

    listQuotations(): any {
        const filter = {
            dateInitialValidity: this.dateInitialValidity,
            dateFinallValidity: this.dateFinallValidity
        };

        this.dollarService.listQuotations(filter)
            .subscribe(res => {
                this.quotations = res;
            });
    }
}
