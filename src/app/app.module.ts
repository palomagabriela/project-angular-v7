import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { ClarityModule } from '@clr/angular';
import { DollarQuotationModule } from './modules/dollar-quotation/index';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ClarityModule,
    UiModule,
    DollarQuotationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
